# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'confiner/version'

Gem::Specification.new do |spec|
  spec.name = 'confiner'
  spec.version = Confiner::VERSION
  spec.authors = ['GitLab Quality']
  spec.email = ['quality+confiner@gitlab.com']

  spec.required_ruby_version = '>= 2.5'

  spec.summary = 'Yaml rule-based confinement'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/confiner'
  spec.license = 'MIT'

  spec.files = `git ls-files -- lib/* bin*`.split("\n")

  spec.bindir = 'bin'
  spec.executables << 'confiner'

  spec.require_paths = ['lib']

  spec.add_development_dependency 'rspec', '~> 3.10.0'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.5.1'

  spec.add_runtime_dependency 'gitlab', '>= 4.17'
  spec.add_runtime_dependency 'zeitwerk', '>= 2.5', '< 3'
end
