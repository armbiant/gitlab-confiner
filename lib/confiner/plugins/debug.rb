# frozen_string_literal: true

module Confiner
  module Plugins
    class Debug < Plugin
      arguments :arg1, :arg2

      def action1
        log :debug, arg1
      end

      def action2
        log :debug, arg2
      end

      def warn
        log :warn, 'Warn'
        log :warning, 'Warning'
      end

      def fatal
        log :fatal, 'Fatal'
      end

      def error
        log :err, 'Err'
        log :error, 'Error'
      end
    end
  end
end
