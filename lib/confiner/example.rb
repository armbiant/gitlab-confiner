# frozen_string_literal: true

module Confiner
  # Representation of an example (test)
  class Example
    attr_accessor :status, :name, :classname, :file, :occurrence

    def initialize(**attributes)
      @status = attributes.fetch('status') { attributes.fetch(:status) }
      @name = attributes.fetch('name') { attributes.fetch(:name) }
      @classname = attributes.fetch('classname') { attributes.fetch(:classname) }
      @file = attributes.fetch('file') { attributes.fetch(:file) }
      @occurrence = attributes.fetch('occurrence') { attributes.fetch(:occurrence) }

      yield(self) if block_given?
    end

    # Check if this example had passed
    # @return [Boolean] true if the example had passed
    def passed?
      status == 'success'
    end

    # Check if this example had failed
    # @return [Boolean] true if the example had failed
    def failed?
      status == 'failed'
    end

    # Check if this example had been skipped
    # @return [Boolean] true if the example had been skipped
    def skipped?
      status == 'skipped'
    end

    def to_s; name; end
  end
end
