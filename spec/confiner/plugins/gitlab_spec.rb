# frozen_string_literal: true

require 'spec_helper'

module Confiner
  module Plugins
    RSpec.describe Gitlab do
      subject(:plugin) do
        described_class.new({ debug: true },
          private_token: 'test-token',
          project_id: '123',
          target_project: 'gitlab-org/quality/confiner',
          failure_issue_labels: 'QA',
          failure_issue_prefix: 'Failure in',
          threshold: 1,
          endpoint: 'http://localhost:3000'
        )
      end

      let(:api) { instance_spy('gitlab api') }
      let(:logger) { instance_spy('confiner logger') }

      let(:all_arguments) do
        %i[
            private_token project_id target_project
            failure_issue_labels failure_issue_prefix
            timeout threshold endpoint pwd ref
          ]
      end

      before do
        Logger.log_to = $stdout
        stub_const('Gitlab::Client', api)
        stub_const('Confiner::Logger', logger)
        allow(api).to receive(:pipelines).and_return(JSON.parse(File.read('spec/fixtures/gitlab-api-mocks/pipelines.json'))) # return 6 pipelines; 3x pass, 3x fail
        allow(api).to receive(:pipeline_test_report).and_return(JSON.parse(File.read('spec/fixtures/gitlab-api-mocks/pipeline_test_report.json')))

        allow(api).to receive(:issues).and_return([
          {
            'web_url' => 'https://gdk.test/issues',
            'iid' => '12345'
          }
        ])
      end

      shared_examples 'fetches pipelines with a given ref' do
        it 'fetches pipelines with a given ref' do
          expect(api).to have_received(:pipelines).twice.with(anything, hash_including(ref: plugin.ref))
        end
      end

      shared_examples 'fetches one pipeline with successful status' do
        it 'fetches one pipeline with successful status' do
          expect(api).to have_received(:pipelines).exactly(1).time.with(anything, hash_including(status: :success))
        end
      end

      shared_examples 'fetches one pipeline with failed status' do
        it 'fetches one pipeline with failed status' do
          expect(api).to have_received(:pipelines).exactly(1).time.with(anything, hash_including(status: :failed))
        end
      end

      shared_examples 'fetches one pipeline with failed status' do
        it 'fetches one pipeline with failed status' do
          expect(api).to have_received(:pipelines).exactly(1).time.with(anything, hash_including(status: :failed))
        end
      end

      shared_examples 'fetches pipelines with per_page specified' do
        it 'fetches pipelines with per_page specified' do
          expect(api).to have_received(:pipelines).twice.with(anything, hash_including(per_page: plugin.threshold * 2)) # per_page is threshold*2
        end
      end

      shared_examples 'fetches pipeline test reports for each pipeline' do
        it 'fetches pipeline test reports for each pipeline' do
          expect(api).to have_received(:pipeline_test_report).exactly(12).times # 12 to fetch 6 passed pipelines and 6 failed
        end
      end

      describe 'arguments' do
        where(:arguments) { all_arguments }

        with_them do
          it 'defines a getter' do
            expect(plugin).to respond_to(arguments)
          end

          it 'defines a setter' do
            expect(plugin).to respond_to("#{arguments}=")
          end
        end
      end

      describe '#initialize' do
        it 'defaults GITLAB_API_HTTPARTY_OPTIONS to set read_timeout', :aggregate_failures do
          plugin # instantiate the plugin

          expect(ENV).to include('GITLAB_API_HTTPARTY_OPTIONS')
          expect(ENV.fetch('GITLAB_API_HTTPARTY_OPTIONS')).to include('read_timeout')
        end

        context 'when arguments are specified'

        context 'when no arguments are specified' do
          it 'raises an error if private_token is not specified' do
            expect { described_class.new({ debug: true }) }.to raise_error(ArgumentError).with_message('Missing private_token')
          end
        end

        it 'instantiates a new gitlab client with a private token and endpoint' do
          plugin

          expect(api).to have_received(:new).with(private_token: plugin.private_token, endpoint: plugin.endpoint)
        end
      end

      describe '#quarantine' do
        before do
          allow(api).to receive(:file_contents).and_return("it 'Failing Test' do")
          allow(api).to receive(:create_branch).and_return({
            'name' => '12345-quarantine' # intentional mismatch of branch name with expected branch names
          })

          plugin.quarantine
        end

        it_behaves_like 'fetches pipelines with a given ref'
        it_behaves_like 'fetches one pipeline with successful status'
        it_behaves_like 'fetches one pipeline with failed status'
        it_behaves_like 'fetches pipelines with per_page specified'
        it_behaves_like 'fetches pipeline test reports for each pipeline'

        context 'when example meets criteria for quarantine' do
          it 'fetches a failure issue for each failed example' do
            expect(api).to have_received(:issues).exactly(6).times # once for each failed issue (6 failed examples in the test report)
          end

          it 'fetches the contents from GitLab for each failed example' do
            expect(api).to have_received(:file_contents).exactly(6).times
          end

          it 'creates a branch with <id>-quarantine-* format', :aggregate_failures do
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-quarantine--Failing-Test-1', plugin.ref)
          end

          it 'commits changes and adds a quarantine tag' do
            expect(api).to have_received(:create_commit).with(plugin.target_project, '12345-quarantine', /Quarantine/, [
              { action: :update, file_path: 'failing_test_1.rb', content: %(it 'Failing Test', quarantine: { issue: 'https://gdk.test/issues', type: :investigating } do\n) }
            ])
          end

          it 'creates a merge request for each test with details', :aggregate_failures do
            # a title with the example's name
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 1', anything)
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 2', anything)
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 3', anything)
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 4', anything)
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 5', anything)
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE]  Failing Test 6', anything)

            # the source and target branch
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, anything, hash_including(source_branch: '12345-quarantine', target_branch: plugin.ref)).exactly(6).times

            # it squashes and removes the source branch after merge
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, anything, hash_including(remove_source_branch: true, squash: true)).exactly(6).times

            # it sets a description
            expect(api).to have_received(:create_merge_request).with(plugin.target_project, anything, hash_including(:description)).exactly(6).times
          end

          context 'when environment information is specified' do
            context 'when information is wrong' do
              before do
                plugin.environment = {
                  'name' => 'testenv'
                }
              end

              it 'errors out' do
                expect { plugin.quarantine }.to raise_error(ArgumentError, /Specify both/)
              end
            end

            context 'when information is ok' do
              subject(:plugin) do
                described_class.new({ debug: true },
                  private_token: 'test-token',
                  project_id: '123',
                  target_project: 'gitlab-org/quality/confiner',
                  failure_issue_labels: 'QA',
                  failure_issue_prefix: 'Failure in',
                  threshold: 1,
                  endpoint: 'http://localhost:3000',
                  environment: {
                    'name' => 'testenv',
                    'pattern' => 'pattern' # intentional ruby 1.9 hash style
                  }
                )
              end

              it 'creates a branch with name that has the environment name' do
                expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-quarantine-testenv-Failing-Test-1', plugin.ref)
              end

              it 'quarantines with an only tag' do
                expect(api).to have_received(:create_commit).with(plugin.target_project, '12345-quarantine', /Quarantine/, [
                  { action: :update, file_path: 'failing_test_1.rb', content: %(it 'Failing Test', quarantine: { issue: 'https://gdk.test/issues', type: :investigating, only: { pattern } } do\n) }
                ])
              end

              it 'creates a merge request with a [ENV] prefix' do
                expect(api).to have_received(:create_merge_request).with(plugin.target_project, '[QUARANTINE] [testenv] Failing Test 1', anything)
              end
            end
          end
        end

        context 'when example does not meet criteria for quarantine'
      end

      describe '#dequarantine' do
        before do
          allow(api).to receive(:file_contents).and_return("it 'Passing Test 1',  quarantine: { issue: 'https://gdk.test/12345', type: :investigating } do")
          allow(api).to receive(:create_branch).and_return({
            'name' => '12345-dequarantine' # intentional mismatch of branch name with expected branch names
          })

          plugin.dequarantine
        end

        it_behaves_like 'fetches pipelines with a given ref'
        it_behaves_like 'fetches one pipeline with successful status'
        it_behaves_like 'fetches one pipeline with failed status'
        it_behaves_like 'fetches pipelines with per_page specified'
        it_behaves_like 'fetches pipeline test reports for each pipeline'

        context 'when example meets criteria for dequarantine' do
          it 'commits changes and removes the quarantine tag' do
            expect(api).to have_received(:create_commit).with(plugin.target_project, '12345-dequarantine', /Dequarantine/, [
              { action: :update, file_path: 'passing_test_1.rb', content: %(it 'Passing Test 1' do\n) }
            ])
          end

          it 'creates a branch with <id>-dequarantine-* format for each of the passing tests' do
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-1', plugin.ref)
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-2', plugin.ref)
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-3', plugin.ref)
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-4', plugin.ref)
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-5', plugin.ref)
            expect(api).to have_received(:create_branch).with(plugin.target_project, '12345-dequarantine--Passing-Test-6', plugin.ref)
          end
        end
      end
    end
  end
end
